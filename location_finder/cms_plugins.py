from django.utils.translation import gettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from location_finder import models
from location_finder.forms import LocationFinderPluginForm
from django.contrib import admin



class LocationFinderColumnInline(admin.TabularInline):
    model = models.LocationFinderColumn
    extra = 0

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@plugin_pool.register_plugin
class LocationFinderPlugin(CMSPluginBase):
    model = models.LocationFinder
    name = _("Location Finder")
    form = LocationFinderPluginForm
    render_template = "location_finder/location_finder.html"
    cache = False
    inlines = [LocationFinderColumnInline,]

    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        context["title"] = instance.title
        context["subtitle"] = instance.subtitle
        context["show_district_borders"] = instance.show_district_borders
        context["show_district_names"] = instance.show_district_names
        context["places"] = [
            place.to_dict()
            for place in models.LocationFinderPlace.objects.filter(location_finder_plugin=instance).select_related(
                "location_finder_plugin"
            ).prefetch_related('location_finder_plugin__locationfindercolumn_set')
        ]
        context["width"] = instance.width
        return context

    def get_readonly_fields(self, request, obj=None):
        fields = super().get_readonly_fields(request, obj)
        if not request.user.has_perm("location_finder.change_plugin_settings"):
            fields += ("title", "subtitle", "show_district_borders", "show_district_names", "width")
        return fields

