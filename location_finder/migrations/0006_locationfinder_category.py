# Generated by Django 3.1.14 on 2023-05-23 06:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('location_finder', '0005_locationfinderplace_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationfinder',
            name='category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='plugins', to='location_finder.locationfindercategory'),
        ),
    ]
