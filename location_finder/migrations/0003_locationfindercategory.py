# Generated by Django 3.1.14 on 2023-05-22 14:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('location_finder', '0002_auto_20230512_1513'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocationFinderCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
            ],
        ),
    ]
