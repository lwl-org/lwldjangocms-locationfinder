from django.db import migrations, models
import django.db.models.deletion
import filer.fields.file


class Migration(migrations.Migration):

    dependencies = [
        ('location_finder', '0012_add_addition_columns_field'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='locationfindercategory',
            options={'verbose_name': 'Kategorie', 'verbose_name_plural': 'Kategorien'},
        ),
        migrations.RemoveField(
            model_name='locationfinder',
            name='category',
        ),
        migrations.AddField(
            model_name='locationfinder',
            name='spreadsheet',
            field=filer.fields.file.FilerFileField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='filer.file', verbose_name='Spreadsheet'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='show_district_borders',
            field=models.BooleanField(default=True, verbose_name='Regionsgrenzen anzeigen'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='show_district_names',
            field=models.BooleanField(default=True, verbose_name='Regionsnamen anzeigen'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='subtitle',
            field=models.CharField(default='Find local history and tourist offices', max_length=255, verbose_name='Untertitel'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='title',
            field=models.CharField(default='Location finder', max_length=255, verbose_name='Titel'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='width',
            field=models.CharField(choices=[('wide', 'Breit'), ('narrow', 'Schmal')], default='wide', max_length=10, verbose_name='Breite'),
        ),
        migrations.CreateModel(
            name='LocationFinderColumn',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('attribute_type', models.CharField(choices=[('other', 'Other'), ('latitude', 'Latitude'), ('longitude', 'Longitude'), ('county', 'County'), ('district', 'District')], default='other', max_length=255)),
                ('location_finder_plugin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='columns', to='location_finder.locationfinder')),
            ],
        ),
    ]
