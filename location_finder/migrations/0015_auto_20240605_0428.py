from django.db import migrations, models
import django.db.models.deletion


def delete_locationfinderplace_objects(apps, schema_editor):
    LocationFinderPlace = apps.get_model('location_finder', 'LocationFinderPlace')
    LocationFinderPlace.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('location_finder', '0014_auto_20240605_0418'),
    ]

    operations = [
        migrations.RunPython(delete_locationfinderplace_objects, migrations.RunPython.noop),
        migrations.AddField(
            model_name='locationfinderplace',
            name='location_finder_plugin',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='location_finder.locationfinder'),
        ),
        migrations.AlterField(
            model_name='locationfindercolumn',
            name='location_finder_plugin',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='location_finder.locationfinder'),
        ),
    ]
