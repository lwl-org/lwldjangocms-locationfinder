from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('location_finder', '0011_auto_20240308_1645'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='locationfindercategory',
            options={'verbose_name': 'Category', 'verbose_name_plural': 'Categories'},
        ),
        #migrations.AddField(
        #    model_name='locationfinderplace',
        #    name='additional_columns',
        #    field=models.TextField(blank=True, default='{}'),
        #),
        migrations.AlterField(
            model_name='locationfinder',
            name='category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='plugins', to='location_finder.locationfindercategory', verbose_name='Category'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='show_district_borders',
            field=models.BooleanField(default=True, verbose_name='Show district borders'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='show_district_names',
            field=models.BooleanField(default=True, verbose_name='Show district names'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='subtitle',
            field=models.CharField(default='Find local history and tourist offices', max_length=255, verbose_name='Subtitle'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='title',
            field=models.CharField(default='Location finder', max_length=255, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='locationfinder',
            name='width',
            field=models.CharField(choices=[('wide', 'Wide'), ('narrow', 'Narrow')], default='wide', max_length=10, verbose_name='Width'),
        ),
    ]
