from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('location_finder', '0015_auto_20240605_0428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locationfindercolumn',
            name='attribute_type',
            field=models.CharField(choices=[('other', 'Other'), ('name', 'Name'), ('latitude', 'Latitude'), ('longitude', 'Longitude'), ('county', 'County'), ('district', 'District'), ('checkbox_filter', 'Checkbox filter')], default='other', max_length=255),
        ),
    ]
