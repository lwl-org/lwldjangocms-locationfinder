from collections import defaultdict

from cms.models import CMSPlugin
from django.db import models
from django.utils.translation import gettext as _
from filer.fields.file import FilerFileField

import chardet
import csv
import io
import json


class LocationFinderPlace(models.Model):
    location_attributes = models.TextField(blank=True, default='{}')
    location_finder_plugin = models.ForeignKey('LocationFinder', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Place"
        verbose_name_plural = "Places"

    def to_dict(self):
        location_attributes = json.loads(self.location_attributes)
        attribute_keys_dict = self.get_attribute_keys_dict()
        return {
            "id": self.id,
            # top info
            "name": self.get_attribute_value(location_attributes, attribute_keys_dict.get('name', [])),
            "image": self.get_attribute_value(location_attributes, attribute_keys_dict.get('image', [])),
            # map
            "latitude": self.get_attribute_value(location_attributes, attribute_keys_dict.get('latitude', [])).replace(
                ',', '.'
            ),
            "longitude": self.get_attribute_value(location_attributes, attribute_keys_dict.get(
                'longitude', []
            )).replace(',', '.'),
            "county": self.get_attribute_value(location_attributes, attribute_keys_dict.get('county', [])),
            "district": self.get_attribute_value(location_attributes, attribute_keys_dict.get('district', [])),
            # postal address
            "address": self.get_attribute_value(location_attributes, attribute_keys_dict.get('address', [])),
            "postal_code": self.get_attribute_value(location_attributes, attribute_keys_dict.get('postal_code', [])),
            "city": self.get_attribute_value(location_attributes, attribute_keys_dict.get('city', [])),
            # sidebar attributes, other
            "sidebar_attributes": self.get_sidebar_attributes(location_attributes, attribute_keys_dict),
            # features
            "checkbox_data": self.get_checkbox_data(location_attributes, attribute_keys_dict),
        }

    def get_checkbox_data(self, location_attributes, attribute_keys_dict):
        return {
            checkbox_key: bool(location_attributes.get(checkbox_key)) for checkbox_key in attribute_keys_dict.get(
                'checkbox_filter', []
            )
        }

    def get_attribute_value(self, attributes_data, attribute_keys):
        try:
            values_list = [attributes_data.get(key, '') for key in attribute_keys]
            return ' '.join(values_list).strip()
        except TypeError:
            return ''

    def get_attribute_keys_dict(self):
        attribute_keys_dict = defaultdict(list)
        for column in self.location_finder_plugin.locationfindercolumn_set.all():
            attribute_keys_dict[column.attribute_type].append(column.name)
        return attribute_keys_dict

    def get_sidebar_attributes(self, location_attributes, attribute_keys_dict):
        return {
            key: value for key, value in location_attributes.items() if key in attribute_keys_dict.get('other', [])
        }


class LocationFinderColumn(models.Model):
    ATTRIBUTE_TYPES = (
        # top info
        ('name', _('Name')),
        ('image', _('Image')),
        # map
        ('latitude', _('Latitude')),
        ('longitude', _('Longitude')),
        ('county', _('County')),
        ('district', _('District')),
        # postal address
        ('address', _('Address')),
        ('postal_code', _('PLZ')),
        ('city', _('City')),
        # sidebar attributes, other
        ('other', _('Other')),
        # features
        ('checkbox_filter', _('Checkbox filter')),
    )
    location_finder_plugin = models.ForeignKey('LocationFinder', on_delete=models.CASCADE)

    name = models.CharField(max_length=255)
    attribute_type = models.CharField(max_length=255, choices=ATTRIBUTE_TYPES, default='other')


class LocationFinder(CMSPlugin):
    spreadsheet = FilerFileField(
        verbose_name=_('Spreadsheet'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    title = models.CharField(
        verbose_name=_('Title'),
        default=_("Location finder"),
        max_length=255
    )
    subtitle = models.CharField(
        verbose_name=_('Subtitle'),
        default=_("Find local history and tourist offices"),
        max_length=255
    )
    show_district_borders = models.BooleanField(
        verbose_name=_('Show district borders'),
        default=True
    )
    show_district_names = models.BooleanField(
        verbose_name=_('Show district names'),
        default=True
    )
    include_district_lippe = models.BooleanField(blank=True, default=True)

    WIDTH_CHOICES = (
        ('wide', _('Wide')),
        ('narrow', _('Narrow')),
    )

    width = models.CharField(
        verbose_name=_('Width'),
        max_length=10,
        choices=WIDTH_CHOICES,
        default=WIDTH_CHOICES[0][0],
    )

    class Meta:
        permissions = (
            ("change_locations", "Can update locations"),
            ("change_plugin_settings", "Can change plugin settings")
        )

    def import_spreadsheet(self):
        self.remove_existing_data()

        csv_file = self.spreadsheet.file
        csv_file.open(mode='rb')

        file_encoding = self.get_encoding(csv_file)
        csv_file_wrapper = io.TextIOWrapper(csv_file, encoding=file_encoding)
        dialect = self.get_dialect(csv_file_wrapper)
        csv_reader = csv.DictReader(csv_file_wrapper, dialect=dialect)

        self.create_columns(csv_reader.fieldnames)
        self.create_location_places(csv_reader)

        csv_file_wrapper.close()
        csv_file.close()

    def get_encoding(self, csv_file):
        sample = csv_file.read(10000)
        result = chardet.detect(sample)
        file_encoding = result['encoding']
        csv_file.seek(0)
        return file_encoding

    def get_dialect(self, csv_file_wrapper):
        sample = csv_file_wrapper.read(10000)
        csv_file_wrapper.seek(0)

        try:
            return csv.Sniffer().sniff(sample)
        except csv.Error:
            delimiters = [',', ';', '\t', '|']
            for delimiter in delimiters:
                try:
                    return csv.Sniffer().sniff(sample, delimiters=[delimiter])
                except csv.Error:
                    continue
            else:
                raise ValueError("Could not determine delimiter")

    def remove_existing_data(self):
        if self.id:
            self.locationfinderplace_set.all().delete()
            self.locationfindercolumn_set.all().delete()

    def create_columns(self, fieldnames):
        for column_name in fieldnames:
            if column_name:
                LocationFinderColumn.objects.get_or_create(
                    location_finder_plugin=self,
                    name=column_name,
                    attribute_type=self.get_default_attribute_type(column_name),
                )

    def get_default_attribute_type(self, column_name):
        # top info
        if column_name.lower() in ['name', 'hvname1']:
            return 'name'
        elif column_name.lower() in ['bild', 'image']:
            return 'image'
        # map
        elif column_name.lower() in ['lat', 'latitude']:
            return 'latitude'
        elif column_name.lower() in ['lng', 'longitude']:
            return 'longitude'
        elif column_name.lower() in ['county', 'kreis', 'krs']:
            return 'county'
        elif column_name.lower() in ['district', 'hg']:
            return 'district'
        # postal address
        elif column_name.lower() in ['address', 'adresse']:
            return 'address'
        elif column_name.lower() in ['plz', 'postleitzahl', 'postalcode']:
            return 'postal_code'
        elif column_name.lower() in ['ort', 'city']:
            return 'city'
        # sidebar attributes, other
        return 'other'

    def create_location_places(self, csv_reader):
        for row in csv_reader:
            LocationFinderPlace.objects.create(
                location_attributes=json.dumps(row),
                location_finder_plugin=self,
            )

    def get_checkbox_filters(self):
        return json.dumps(list(LocationFinderColumn.objects.filter(
            location_finder_plugin=self, attribute_type='checkbox_filter'
        ).values_list('name', flat=True)))

    def copy_relations(self, old_instance):
        super().copy_relations(old_instance)
        for column in old_instance.locationfindercolumn_set.all():
            column.pk = None
            column.location_finder_plugin = self
            column.save()
        for place in old_instance.locationfinderplace_set.all():
            place.pk = None
            place.location_finder_plugin = self
            place.save()
