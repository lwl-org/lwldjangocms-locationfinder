const e = "Alles", t = "Standorte", n = "Suchen", s = {
  all: e,
  "clear-all": "Alles löschen",
  locations: t,
  "locations-found": "Standorte gefunden",
  "no-results-matching-query": "Keine Ergebnisse für die Abfrage",
  search: n,
  "search-term": "Suchbegriff",
  "select-region": "Region wählen",
  "start-typing": "Afangen zu tippen",
  "town-street-etc": "Stadt, Straße usw.",
  "view-list": "Liste anzeigen"
};
export {
  e as all,
  s as default,
  t as locations,
  n as search
};
