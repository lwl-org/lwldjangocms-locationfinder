const e = "Alles", t = "Standorte", n = "Bereich", s = "Suchen", a = {
  all: e,
  "clear-all": "Alles löschen",
  locations: t,
  "locations-found": "Standorte gefunden",
  "no-results-matching-query": "Keine Ergebnisse für die Abfrage",
  region: n,
  search: s,
  "search-term": "Suchbegriff",
  "select-region": "Bereich auswählen",
  "start-typing": "Afangen zu tippen...",
  "town-street-etc": "Stadt, straße usw.",
  "view-list": "Liste anzeigen"
};
export {
  e as all,
  a as default,
  t as locations,
  n as region,
  s as search
};
