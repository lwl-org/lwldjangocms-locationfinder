const t = "All", e = "Locations", o = "Search", a = {
  all: t,
  "clear-all": "Clear all",
  locations: e,
  "locations-found": "Locations found",
  "no-results-matching-query": "No results matching query",
  search: o,
  "search-term": "Search term",
  "select-region": "Select region",
  "start-typing": "Start typing",
  "town-street-etc": "Town, Street etc.",
  "view-list": "View list"
};
export {
  t as all,
  a as default,
  e as locations,
  o as search
};
