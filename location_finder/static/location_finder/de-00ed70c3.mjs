const e = "Alles", n = "Standorte", t = "Bereich", s = "Suchen", a = {
  all: e,
  "clear-all": "Alles löschen",
  locations: n,
  "locations-found": "Standorte gefunden",
  "no-results-matching-query": "Keine Ergebnisse für die Abfrage",
  region: t,
  search: s,
  "search-term": "Suchbegriff",
  "select-region": "Bereich auswählen",
  "start-typing": "Anfangen zu tippen...",
  "town-street-etc": "Stadt, Straße usw.",
  "view-list": "Liste anzeigen"
};
export {
  e as all,
  a as default,
  n as locations,
  t as region,
  s as search
};
