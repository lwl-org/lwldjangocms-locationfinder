const e = "Suchen", t = {
  "select-region": "Region wählen",
  "start-typing": "Afangen zu tippen",
  "town-street-etc": "Stadt, Straße usw.",
  search: e,
  "clear-all": "Alles löschen",
  "no-results-matching-query": "Keine Ergebnisse für die Abfrage"
};
export {
  t as default,
  e as search
};
