const e = "Search", t = {
  "select-region": "Select region",
  "start-typing": "Start typing",
  "town-street-etc": "Town, Street etc.",
  search: e,
  "clear-all": "Clear all",
  "no-results-matching-query": "No results matching query"
};
export {
  t as default,
  e as search
};
