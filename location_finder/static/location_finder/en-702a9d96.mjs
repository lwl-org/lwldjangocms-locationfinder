const e = "All", t = "Search", l = {
  all: e,
  "select-region": "Select region",
  "start-typing": "Start typing",
  "town-street-etc": "Town, Street etc.",
  search: t,
  "clear-all": "Clear all",
  "no-results-matching-query": "No results matching query"
};
export {
  e as all,
  l as default,
  t as search
};
