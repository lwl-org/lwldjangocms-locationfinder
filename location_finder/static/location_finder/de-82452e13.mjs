const e = "Alles", n = "Standorte", t = "Bereich", s = "Suchen", r = {
  all: e,
  "clear-all": "Alles löschen",
  locations: n,
  "locations-found": "Standorte gefunden",
  "no-results-matching-query": "Keine Ergebnisse für die Abfrage",
  region: t,
  search: s,
  "search-term": "Suchbegriff",
  "select-region": "Bereich auswählen",
  "start-typing": "Anfangen zu tippen...",
  "town-street-etc": "Stadt, Straße usw.",
  "view-list": "Liste anzeigen",
  "no-items-found": "Keine Artikel gefunden"
};
export {
  e as all,
  r as default,
  n as locations,
  t as region,
  s as search
};
