const e = "Alles", t = "Suchen", n = {
  all: e,
  "select-region": "Region wählen",
  "start-typing": "Afangen zu tippen",
  "town-street-etc": "Stadt, Straße usw.",
  search: t,
  "clear-all": "Alles löschen",
  "no-results-matching-query": "Keine Ergebnisse für die Abfrage"
};
export {
  e as all,
  n as default,
  t as search
};
