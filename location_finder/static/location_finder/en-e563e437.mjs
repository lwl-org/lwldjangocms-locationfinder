const t = "All", e = "Locations", o = "Region", n = "Search", s = {
  all: t,
  "clear-all": "Clear all",
  locations: e,
  "locations-found": "Locations found",
  "no-results-matching-query": "No results matching query",
  region: o,
  search: n,
  "search-term": "Search term",
  "select-region": "Select region",
  "start-typing": "Start typing...",
  "town-street-etc": "Town, street etc.",
  "view-list": "View list"
};
export {
  t as all,
  s as default,
  e as locations,
  o as region,
  n as search
};
