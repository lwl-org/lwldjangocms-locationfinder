from django.http import HttpResponse

from .models import LocationFinderPlace

import json


def places(request, location_finder_id=None):
    places_qs = LocationFinderPlace.objects.filter(location_finder_plugin_id=location_finder_id).select_related(
        "location_finder_plugin"
    ).prefetch_related('location_finder_plugin__locationfindercolumn_set')
    places_dicts_list = [place.to_dict() for place in places_qs]
    return HttpResponse(json.dumps(places_dicts_list), content_type="application/json")
