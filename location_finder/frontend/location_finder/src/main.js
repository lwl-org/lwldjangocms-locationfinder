import { createApp } from 'vue'

import App from './App.vue'
import 'leaflet/dist/leaflet.css'
import './assets/sass/main.scss'
import GlobalComponents from './globalComponents'
import i18n from './plugins/i18n';

const app = createApp(App)

app.use(GlobalComponents)
app.use(i18n())
app.mount('#location_finderplugin')

export default app
