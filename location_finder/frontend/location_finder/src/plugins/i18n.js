import { nextTick } from 'vue';
import { createI18n } from 'vue-i18n';

let i18n;

export const SUPPORT_LOCALES = ['en', 'de'];

export function setI18nLanguage(locale) {
  loadLocaleMessages(locale);

  if (i18n.mode === 'legacy') {
    i18n.global.locale = locale;
  } else {
    i18n.global.locale.value = locale;
  }
}

export async function loadLocaleMessages(locale) {
  // load locale messages with dynamic import
  const messages = await import(
    `../locales/${locale}.json`
  );
  // set locale and locale message
  i18n.global.setLocaleMessage(locale, messages.default);

  return nextTick();
}

export default function setupI18n() {
	if (!i18n) {
		// when Django changes the language, we can get it from this attribute
		// and then we can update the locale for the i18n instance used by Vue
		let locale = document.documentElement.getAttribute('lang') || 'de';

		i18n = createI18n({
			globalInjection: true,
			legacy: false,
			locale: locale,
			fallbackLocale: 'de'
		});

		setI18nLanguage(locale);
	};

  return i18n;
}
