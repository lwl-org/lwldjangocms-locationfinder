from django.urls import re_path

from .views import places


urlpatterns = [
    re_path("^location-finder/places/(?P<location_finder_id>\d+)/$", places, name="places"),
]
