from django import forms
from location_finder.models import LocationFinder


class LocationFinderPluginForm(forms.ModelForm):
    class Meta:
        model = LocationFinder
        fields = '__all__'

    def save(self, commit=True):
        is_spreadsheet_new = self.is_spreadsheet_new()
        instance = super().save(commit=True)
        print('*********************************')
        print(instance)
        print(instance.id)
        print('*********************************')
        if is_spreadsheet_new:
            instance.import_spreadsheet()
        return instance

    def save_m2m(self):
        pass

    def is_spreadsheet_new(self):
        spreadsheet_id = getattr(self.cleaned_data.get('spreadsheet'), 'id', None);
        return spreadsheet_id and (
            not self.instance.id or spreadsheet_id != LocationFinder.objects.get(id=self.instance.id).spreadsheet_id
        )
