from setuptools import setup, find_packages
from location_finder import __version__


setup(
    name='lwldjangocms-locationfinder',
    version=__version__,
    description='LWL Location Finder',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    author='what.digital',
    author_email='mario@what.digital',
    packages=find_packages(),
    platforms=['OS Independent'],
    install_requires=[],
    url='https://gitlab.com/lwl-org/lwldjangocms-locationfinder',
    include_package_data=True,
    zip_safe=False,
)
