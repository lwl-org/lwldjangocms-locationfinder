# Location Finder Plugin

This python module is open-source, available here: https://gitlab.com/lwl-org/lwldjangocms-locationfinder


## Overview

Location Finder is a Django app integrating Open Street Map for easy location searching and pinning. Users can add a Django CMS plugin to display an interactive map, search for locations, and view details about them.


## Setup

Install the package by using a direct link to the commit you need. For example:
```
pip install https://gitlab.com/lwl-org/lwldjangocms-locationfinder/-/archive/35ba3c3a317cf323d248e3687491b51c1d383bfa/lwldjangocms-locationfinder-35ba3c3a317cf323d248e3687491b51c1d383bfa.tar.gz
```

Add the following to your `settings.py`: 

```
INSTALLED_APPS = [
    'location_finder',
]
```

Run the migrations
```
python manage.py migrate
```


## Usage

### Adding Location Categories

1. In Django admin, create a Location finder category.
2. Attach a .csv file with location data. The columns should be:
```
HvName1, HvName2, GH, OH, SH, Och, VHV, Krshpf, Titel, Vorname, Name1, Name2, Name3, Str, Plz, Ort, Telpriv, Hg, Krs, Stadt, Ortsteil, Email1, Email2, Email3, Lat, Lng
```
- The first row is for column titles.
- Each subsequent row represents a location.

### Adding the Plugin to a Page

1. Add a 'Location finder' plugin on your page.
2. Select the desired category. Locations from this category will be available on the map.

### Features

- Interactive Map: Users can view locations as pins on the map.
- Search Functionality: Search for locations using the sidebar. Results are shown as pins on the map, and menu options in the sidebar.
- Location Details: Clicking a pin or a result in the left sidebar opens a right sidebar with more information about the location.

### Colour Styles

The plugin is designed to inherit the colours used by the DjangoCMS it is installed into (eg. SCSS vars such as `primary-base`).
Therefore when running locally using `npm run dev` some elements (like borders on controls or buttons) might appear white or transparent
unless you uncomment the vars at the top of main.scss. **Remember to re-comment them before checking in code changes.**
